### Problem to solve

<!--- What problem do we solve? -->

### Intended users

<!-- Who will use this feature? If known, include any of the following: types of users (e.g. Developer), personas, or specific company roles (e.g. Release Manager). It's okay to write "Unknown" and fill this field in later.
Personas can be found at https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/ -->

### Further details

<!--- Include use cases, benefits, and/or goals (contributes to our vision?) -->

### Proposal

<!--- How are we going to solve the problem? Try to include the user journey! -->

### What does success look like, and how can we measure that?

<!--- Define both the success metrics and acceptance criteria. Note thet success metrics indicate the desired business outcomes, while acceptance criteria indicate when the solution is working correctly. If there is no way to measure success, link to an issue that will implement a way to measure this -->

### Links / references

cc @gl-website
