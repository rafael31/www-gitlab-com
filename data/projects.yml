gitlab:
  name: GitLab
  path: gitlab-org/gitlab
  link: https://gitlab.com/gitlab-org/gitlab
  mirrors:
    - https://dev.gitlab.org/gitlab/gitlabhq
  description: |
    This is the canonical development repository for GitLab. Open source code
    from this repository gets mirrored to GitLab-FOSS project.

    This project is the source of the gitlab-ee distributions like
    omnibus-gitlab packages, docker images, AWS AMIs etc.

    GitLab EE requires a license key to be used, without which only the CE
    functionality will be available.

gitlab-foss:
  name: GitLab FOSS
  path: gitlab-org/gitlab-foss
  link: https://gitlab.com/gitlab-org/gitlab-foss
  mirrors:
    - https://github.com/gitlabhq/gitlabhq
    - https://dev.gitlab.org/gitlab/gitlabhq
  description: |
    This is a mirror of GitLab codebase with all the proprietary code removed.

    Development happens in the canonical repository at GitLab.
    This repository is updated periodically with changes from there.

    This project is the source of the gitlab-ce distributions like
    omnibus-gitlab packages, docker images, AWS AMIs etc.

gitlab-elasticsearch-indexer:
  name: GitLab Elasticsearch Indexer
  path: gitlab-org/gitlab-elasticsearch-indexer
  link: https://gitlab.com/gitlab-org/gitlab-elasticsearch-indexer
  description: |
    An Elasticsearch indexer for Git repositories. Used by GitLab EE to
    implement Global Code Search.

gitlab-shell:
  name: GitLab Shell
  path: gitlab-org/gitlab-shell
  link: https://gitlab.com/gitlab-org/gitlab-shell
  mirrors:
    - https://dev.gitlab.org/gitlab/gitlab-shell
    - https://github.com/gitlabhq/gitlab-shell
  description: |
    GitLab Shell handles Git commands for GitLab. It's an essential part of GitLab.

gitlab-workhorse:
  name: GitLab Workhorse
  path: gitlab-org/gitlab-workhorse
  link: https://gitlab.com/gitlab-org/gitlab-workhorse
  description: |
    Gitlab-workhorse is a smart reverse proxy for GitLab. It handles "large" HTTP
    requests such as file downloads, file uploads, Git push/pull and Git archive
    downloads.

omnibus-gitlab:
  name: Omnibus GitLab
  path: gitlab-org/omnibus-gitlab
  link: https://gitlab.com/gitlab-org/omnibus-gitlab
  mirrors:
  - https://dev.gitlab.org/gitlab/omnibus-gitlab
  - https://github.com/gitlabhq/omnibus-gitlab
  description: |
    Omnibus GitLab creates the packages for GitLab.

cookbook-omnibus-gitlab:
  name: Cookbook Omnibus GitLab
  path: gitlab-org/cookbook-omnibus-gitlab
  link: https://gitlab.com/gitlab-org/cookbook-omnibus-gitlab
  description: |
    Chef Cookbooks for deploying omnibus-gitlab package

distribution:
  name: Distribution team issue tracker
  path: gitlab-org/distribution/team-tasks
  link: https://gitlab.com/gitlab-org/distribution/team-tasks
  description: |
    Used to track Distribution team work.

gitlab-com-infrastructure:
  name: GitLab.com - infrastructure Terraform files
  path: gitlab-com/gitlab-com-infrastructure
  link: https://gitlab.com/gitlab-com/gitlab-com-infrastructure
  description: |
    Terraform - configuration and provisioning files for virtual machine nodes on production and staging

gitlab-com-runbooks:
  name: Gitlab.com - "on call" runbooks
  path: gitlab-com/runbooks
  link: https://gitlab.com/gitlab-com/runbooks
  description: |
    Describes system components, triage procedures, and commands to use in scenarios commonly faced
    by GitLab.com production support engineers

infrastructure:
  name: GitLab.com - infrastructure issue tracker
  path: gitlab-com/infrastructure
  link: https://gitlab.com/gitlab-com/infrastructure
  description: |
    Used to track the infrastructure work of GitLab.com itself

gitlab-cookbooks:
  name: GitLab.com - infrastructure node provisioning by role
  path: gitlab-cookbooks
  link: https://gitlab.com/gitlab-cookbooks
  description: |
    This is a group with a project - cookbooks - for each provisioned role in the GitLab.com cluster.
    These cooksbooks are applied after the virtual machine node is provisioned by Terraform project.

gitlab-cog:
  name: GitLab.com COGS
  path: gitlab-cog
  link: https://gitlab.com/gitlab-cog
  description: |
    This is a group which contains a separate project for each COG used by the GitLab.com infrastructure.

    A COG is an integration between the role of a node (or cluster) and a Chat application such as Slack.  The COG allows
    the infrastructure node (or cluster) to post status and alerts into Chat Channels and also allows for commands
    to be issued in the Chat Channel that control the behavior of the infrastructure node/cluster.

gitlab-development-kit:
  name: GitLab Development Kit
  path: gitlab-org/gitlab-development-kit
  link: https://gitlab.com/gitlab-org/gitlab-development-kit
  description: |
    GitLab Development Kit (GDK) provides a collection of scripts and other
    resources to install and manage a GitLab installation for development
    purposes. The source code of GitLab is spread over multiple repositories
    and it requires Ruby, Go, Postgres/MySQL, Redis and more to run.

    GDK helps you install and configure all these different components,
    and start/stop them when you work on GitLab.

gitaly:
  name: Gitaly
  path: gitlab-org/gitaly
  link: https://gitlab.com/gitlab-org/gitaly
  description: |
    Git RPC service for handling all the git calls made by GitLab.

gitlab-qa:
  name: GitLab QA
  path: gitlab-org/gitlab-qa
  link: https://gitlab.com/gitlab-org/gitlab-qa
  description: |
    End-to-end, black-box, entirely click-driven integration tests for GitLab.

gitlab-triage:
  name: GitLab Triage
  path: gitlab-org/gitlab-triage
  link: https://gitlab.com/gitlab-org/gitlab-triage
  description: |
    This gem aims to enable project managers and maintainers to automatically
    triage Issues and Merge Requests in GitLab projects based on defined
    policies.

triage-ops:
  name: GitLab triage operations
  path: gitlab-org/quality/triage-ops
  link: https://gitlab.com/gitlab-org/quality/triage-ops
  description: |
    Triage operations for GitLab Issues and Merge Requests

gitlab-styles:
  name: GitLab Styles
  path: gitlab-org/gitlab-styles
  link: https://gitlab.com/gitlab-org/gitlab-styles
  description: |
    This gem centralizes some shared GitLab's styles config (only RuboCop
    for now), as well as custom RuboCop cops.

www-gitlab-com:
  name: GitLab Inc. Homepage
  path: gitlab-com/www-gitlab-com
  link: https://gitlab.com/gitlab-com/www-gitlab-com
  description: GitLab Inc. Homepage available at about.GitLab.com

marketo-tools:
  name: Marketo Tools
  path: gitlab-com/marketo-tools
  link: https://gitlab.com/gitlab-com/marketo-tools
  description: Internal Marketo Tools

githost:
  name: GitHost.io
  path: gitlab-com/githost
  link: https://gitlab.com/gitlab-com/githost
  description: Hosted version of GitLab

gitlab-pages:
  name: GitLab Pages
  path: gitlab-org/gitlab-pages
  link: https://gitlab.com/gitlab-org/gitlab-pages
  description: |
    GitLab Pages daemon used to serve static websites for GitLab users

gitlab-runner:
  name: GitLab Runner
  path: gitlab-org/gitlab-runner
  link: https://gitlab.com/gitlab-org/gitlab-runner
  description: GitLab CI/CD Runner

license-app:
  name: License App
  path: gitlab-com/license-gitlab-com
  link: https://gitlab.com/gitlab-com/license-gitlab-com
  description: Internal GitLab License App

customers-app:
  name: Customers App (Subscription Portal)
  path: gitlab-come/customers-gitlab-com
  link: https://gitlab.com/gitlab-com/customers-gitlab-com
  description: Internal GitLab Customers App

gitlab-license:
  name: GitLab License
  path: gitlab/gitlab-license
  link: https://dev.gitlab.org/gitlab/gitlab-license
  description: Internal GitLab License Distribution App

university:
  name: GitLab University
  path: gitlab-org/university
  link: https://gitlab.com/gitlab-org/university
  description: Internal GitLab University

version-gitlab-com:
  name: version.gitlab.com
  path: gitlab-com/version-gitlab-com
  link: https://gitlab.com/gitlab-com/version-gitlab-com
  description: Internal GitLab Version App

gitlab-contributors:
  name: GitLab Contributors
  path: gitlab-com/gitlab-contributors
  link: https://gitlab.com/gitlab-com/gitlab-contributors
  description: Application behind contributors.gitlab.com

gitlab-templates:
  name: GitLab CI/CD Templates
  path: gitlab-org/gitlab-ci-yml
  link: https://gitlab.com/gitlab-org/gitlab-ci-yml
  description: GitLab CI/CD Templates

pages-gitlab-io:
  name: pages.gitlab.io
  path: pages/pages.gitlab.io
  link: https://gitlab.com/pages/pages.gitlab.io
  description: GitLab Pages landing page

gitlab-anti-spam-toolkit:
  name: GitLab Anti-Spam Toolkit
  path: MrChrisW/gitlab-anti-spam-toolkit
  link: https://gitlab.com/MrChrisW/gitlab-anti-spam-toolkit
  description: GitLab Anti-Spam Toolkit

gitlab-monitor:
  name: GitLab Monitor
  path: gitlab-org/gitlab-monitor
  link: https://gitlab.com/gitlab-org/gitlab-monitor
  description: Tooling used to monitor GitLab.com

gitlab-docs:
  name: GitLab Docs
  path: gitlab-org/gitlab-docs
  link: https://gitlab.com/gitlab-org/gitlab-docs
  description: Project behind docs.GitLab.com

gitlab_kramdown:
  name: GitLab Kramdown
  path: gitlab-org/gitlab_kramdown
  link: https://gitlab.com/gitlab-org/gitlab_kramdown
  description: GitLab Flavored Markdown extensions to Kramdown

gitlab-markup:
  name: GitLab Markup
  path: gitlab-org/gitlab-markup
  link: https://gitlab.com/gitlab-org/gitlab-markup
  description: Markup render for non Markdown content

release-tools:
  name: GitLab Release Tools
  path: gitlab-org/release-tools
  link: https://gitlab.com/gitlab-org/release-tools
  description: Instructions and tools for releasing GitLab

takeoff:
  name: takeoff
  path: gitlab-org/takeoff
  link: https://gitlab.com/gitlab-org/takeoff
  description: Tooling used to deploy GitLab.com to any environment

spam-master:
  name: Spam-Master
  path: gitlab-com/spam-master
  link: https://gitlab.com/gitlab-com/spam-master
  description: |
    Collection of spam fighting API scripts for GitLab instances

rubocop-gitlab-security:
  name: RuboCop-GitLab-Security
  path: gitlab-org/rubocop-gitlab-security
  link: https://gitlab.com/gitlab-org/rubocop-gitlab-security
  description: |
    GitLab RuboCop gem for static analysis.

gitlab-design:
  name: GitLab Design
  path: gitlab-org/gitlab-design
  link: https://gitlab.com/gitlab-org/gitlab-design
  description:
    GitLab Design is used to jumpstart design work through the use of our design library.
    It is intended to enable frequent, stable, and consistent contributions while making
    GitLab's design open and transparent. This project helps facilitate design handoffs and
    design–development communication.

charts-gitlab-io:
  name: GitLab Helm Charts
  path: charts/charts.gitlab.io
  link: https://gitlab.com/charts/charts.gitlab.io
  description: |
    GitLab's official All-in-one Helm charts.

gitlab-chart:
  name: GitLab Cloud Native Helm Chart
  path: charts/gitlab
  link: https://gitlab.com/charts/gitlab
  description: |
    GitLab's official Cloud Native Helm chart.

charts-auto-deploy-app:
  name: Auto Deploy App Helm Chart
  path: charts/auto-deploy-app
  link: https://gitlab.com/gitlab-org/charts/auto-deploy-app
  description: |
    Used by Auto DevOps as a template for deploying the applciations.

kubernetes-gitlab-demo:
  name: Kubernetes GitLab Demo
  path: gitlab-org/kubernetes-gitlab-demo
  link: https://gitlab.com/gitlab-org/kubernetes-gitlab-demo
  description: |
    Deprecated GitLab Idea to Production Kubernetes demo project

group-conversations:
  name: Group Conversations
  path: gitlab-org/group-conversations
  link: https://gitlab.com/gitlab-org/group-conversations
  description: |
    Presentations from (some of) the teams at GitLab, to update the rest of the
    world on what they've been working on.

design.gitlab.com:
  name: Pajamas Design System
  path: gitlab-org/gitlab-services/design.gitlab.com
  link: https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com
  description: |
    GitLab's open source Design System. Contains brand and product design
    guidelines and UI components for all things GitLab. 

gitlab-svgs:
  name: GitLab SVGs
  path: gitlab-org/gitlab-svgs
  link: https://gitlab.com/gitlab-org/gitlab-svgs
  description: |
    Our SVG Assets and the corresponding pipelines to create all of our SVG sprites
    for our icons. Also automatic optimisation of SVG based illustrations.

gitlab-restore:
  name: Backup/restore procedures
  path: gitlab-restore
  link: https://gitlab.com/gitlab-restore
  description: |
    Automated backup and restore procedures for GitLab.com data

tanukidesk:
  name: Tanukidesk
  path: gitlab-com/marketing/community-relations/community-advocacy/tanukidesk
  link: https://gitlab.com/gitlab-com/marketing/community-relations/community-advocacy/tanukidesk
  description: |
    Bidirectional communication between Zendesk and Disqus / HackerNews.

remote-only:
  name: RemoteOnly.org
  path: gitlab-com/www-remoteonly-org
  link: https://gitlab.com/gitlab-com/www-remoteonly-org/
  description: |
    This is the source for the https://www.remoteonly.org site.

grape-path-helpers:
  name: grape-path-helpers
  path: gitlab-org/grape-path-helpers
  link: https://gitlab.com/gitlab-org/grape-path-helpers
  description: |
    Provides named route helpers for Grape APIs, similar to Rails' route helpers.
    Forked/renamed from https://github.com/reprah/grape-route-helpers

gitlab-ui:
  name: gitlab-ui
  path: gitlab-org/gitlab-ui
  link: https://gitlab.com/gitlab-org/gitlab-ui
  description: |
    UI component library written in Vue.js.
    Use https://gitlab-org.gitlab.io/gitlab-ui/ to see all the components.
    Every frontend engineer at GitLab is a reviewer of this project.

gitlab-eslint-config:
  name: gitlab-eslint-config
  path: gitlab-org/gitlab-eslint-config
  link: https://gitlab.com/gitlab-org/gitlab-eslint-config
  description: |
    GitLab's eslint configuration

prometheus-client-mmap:
  name: prometheus-client-mmap
  path: gitlab-org/prometheus-client-mmap
  link: https://gitlab.com/gitlab-org/prometheus-client-mmap
  description: |
    This Prometheus library is fork of Prometheus Ruby Client
    that uses memory-mapped files to share metrics from multiple processes

gitlab-build-images:
  name: gitlab-build-images
  path: gitlab-org/gitlab-build-images
  link: https://gitlab.com/gitlab-org/gitlab-build-images/
  description: |
    A repository for building Docker images for GitLab builds and tests.

gitlab-omnibus-builder:
  name: GitLab Omnibus Builder
  path: gitlab-org/gitlab-omnibus-builder
  link: https://gitlab.com/gitlab-org/gitlab-omnibus-builder
  description: |
    A repository for building base Docker images to be used while building
    Omnibus-GitLab packages for supported operating systems. It also contains
    a cookbook to setup builder machines.

labkit:
  name: labkit
  path: gitlab-org/labkit
  link: https://gitlab.com/gitlab-org/labkit
  description: |
    LabKit is minimalist library to provide functionality for Go services at GitLab.

labkit-ruby:
  name: labkit-ruby
  path: gitlab-org/labkit-ruby
  link: https://gitlab.com/gitlab-org/labkit-ruby
  description: |
    LabKit-Ruby is minimalist library to provide functionality for Ruby services at GitLab.

gitlabktl:
  name: gitlabktl
  path: gitlab-org/gitlabktl
  link: https://gitlab.com/gitlab-org/gitlabktl
  description: |
    `gitlabktl` is a tool that integrates GitLab with Kubernetes / Knative and
    is a part of GitLab Serverless platform.

gitter-webapp:
  name: Gitter webapp
  path: gitlab-org/gitter/webapp
  link: https://gitlab.com/gitlab-org/gitter/webapp
  description: |
    Gitter is a community for software developers. This project is the main monolith web chat application.

meltano:
  name: Meltano
  path: meltano/meltano
  link: https://gitlab.com/meltano/meltano
  description: |
    Meltano is an open source convention-over-configuration product for the whole data lifecycle, all the way from loading data to analyzing it.
    It does data ops, data engineering, analytics, business intelligence, and data science.
    
    Meltano stands for the steps of the data science life-cycle: Model, Extract, Load, Transform, Analyze, Notebook, and Orchestrate.

peopleops-bot:
  name: PeopleOps Automation Bot
  path: gitlab-com/people-ops/employment
  link: https://gitlab.com/gitlab-com/people-ops/employment
  description: |
    Bot to automate common PeopleOps operations like onboarding issue
    creation, anniversary and new hire announcements, general issue
    housekeeping, etc.

snowflake_spend:
  name: Snowflake Spend dbt Package
  path: gitlab-data/snowflake_spend
  link: https://gitlab.com/gitlab-data/snowflake_spend
  description: |
    This is a dbt package for understanding the cost your Snowflake Data Warehouse is accruing.

gitlab-data-utils:
  name: Data Utils
  path: gitlab-data/gitlab-data-utils
  link: https://gitlab.com/gitlab-data/gitlab-data-utils
  description: |
    Repo for commonly used utilities within the data org

data-image:
  name: Data Infrastructure
  path: gitlab-data/data-image
  link: https://gitlab.com/gitlab-data/data-image
  description: |
    The project for the Data Team's Docker Images/Kubernetes Infrastructure

gitlab_data:
  name: GitLab Data
  path: gitlab-data/analytics
  link: https://gitlab.com/gitlab-data/analytics
  description: |
    The primary project for the GitLab Data team

data_chatops:
  name: GitLab Data Chatops
  path: gitlab-data/chatops
  link: https://gitlab.com/gitlab-data/chatops
  description: |
    The Data team primarily uses chatops to troubleshoot data quality concerns.
