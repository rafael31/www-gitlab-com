---
layout: markdown_page
title: "Global Content Team"
---

## On this page
{:.no_toc}

- TOC
{:toc}

The Content team includes audience development, editorial, social marketing, video production, and content strategy, development, and operations. The Content Marketing team is responsible for the stewardship of GitLab's audiences, users, customers, and partners' content needs. Content marketing creates engaging, inspiring, and relevant content, executing integrated content programs to deliver useful and cohesive content experiences that build trust and preference for GitLab.

Roles on the Content Marketing team include:

- [Editorial](/job-families/marketing/editor/)
- [Content Marketing](/job-families/marketing/content-marketing/)
- [Digital Production](/job-families/marketing/digital-production-manager/)
- [Social Marketing](/job-families/marketing/social-marketing-manager/)

## Quick links

- [Blog calendar](/handbook/marketing/blog/#blog-calendar)
- [Content marketing schedule](/handbook/marketing/corporate-marketing/content/schedule/)
- [Digital production handbook](/handbook/marketing/corporate-marketing/content/digital-production/)
- [Editorial team page (including blog style guide)](/handbook/marketing/corporate-marketing/content/editorial-team)
- [GitLab blog](/handbook/marketing/blog)
- [Newsletters ](/handbook/marketing/marketing-sales-development/marketing-programs/#newsletter)
- [User spotlights](/handbook/marketing/corporate-marketing/content/user-spotlights)

## Mission & vision

Our content marketing mission statement mirrors our [company mission](/company/strategy/#mission). We strive to foster an open, transparent, and collaborative world where all digital creators can be active participants regardless of location, skillset, or status. This is the place we share our community's success and learning, helpful information and advice, and inspirational insights.

Our vision is to build the largest and most diverse community of cutting edge co-conspirators who are defining and creating the next generation of software development practices. Our plan to turn our corporate blog in to a digital magazine will allow us to add breadth, depth and support to our participation in and coverage of this space.

## Content team responsibilities

- Content pillar production
- Customer content creation
- Video production
- Blog management including writing, editing, and scheduling
- Branded YouTube management
- Organic branded social media mangement (Twitter, Facebook, and LinkedIn)
- Social media campaigns

## Communication

**Chat**

Please use the following Slack channels:

- `#content` for general inquiries
- `#content-updates` for updates and log of all new, published content
- `#blog` RSS feed
- `#twitter` for questions about social
- `#content-hack-day` for updates and information on Content Hack Day

**Issue trackers**
 - [Blog](https://gitlab.com/gitlab-com/www-gitlab-com/boards/804552?&label_name[]=blog%20post)
 - [Content by stage](https://gitlab.com/groups/gitlab-com/-/boards/1136104)
 - [Content Marketing](https://gitlab.com/gitlab-com/marketing/corporate-marketing/boards/911769?&label_name[]=Content%20Marketing)
 - [Digital production](https://gitlab.com/gitlab-com/marketing/corporate-marketing/boards/1120979?&label_name[]=Video%20project)
 - [Social marketing](https://gitlab.com/gitlab-com/marketing/corporate-marketing/boards/968633?&label_name[]=Social)

## Requesting content and copy reviews

1. If you are looking for content to include in an email, send to a customer, or share on social, check the [GitLab blog](/blog/) first.
1. If you need help finding relevant content to use, ask for help in the #content channel.
1. If the content you're looking for doesn't exist and it's a:
   1. Blog post: See the [blog handbook](/handbook/marketing/blog)
   1. Digital asset (eBook, infographic, report, etc.): open an issue in the [corporate marketing project](https://gitlab.com/gitlab-com/marketing/corporate-marketing/) and label it `content marketing`
   1. Video: open an issue in the [corporate marketing project](https://gitlab.com/gitlab-com/marketing/corporate-marketing/) and label it `video production`
1. If you need a copyedit, ping @erica. Please give at least 3 days' notice.

## Content production

The content team supports many cross-functional marketing initiatives. We aim for content production to be a month ahead of planned events and campaigns so that content is readily available for use. In accordance with our values of [iteration](/handbook/values/#iteration) and [transparency](/handbook/values/#transparency), we publish our proposed content plans at the beginning of each quarter. We don't hoard content for one big launch and instead plan sets of content to execute on each quarter and published each piece of content as it's completed.

Content production is determined and prioritized based on the following:

1. Corporate GTM themes
1. Integrated campaigns
1. Corporate marketing events
1. Newsworthiness
1. Brand awareness opportunity

We align our content production to pillars and topics to ensure we're creating a cohesive set of content for our audience to consume. Pillar content is multi-purpose and can be plugged into integrated campaigns, event campaigns, and sales plays.

### Content alignment 

Content production is aligned to digital campaigns and product messaging. 

**Definitions**

- Campaign/Value driver: A high-level GTM message that doesn't change often. Campaigns are tracked as `Parent Epics`.  
- Pillar: A story within a theme. Pillars are tracked as `Child Epics`.
- Set: A topical grouping of content to be executed within a specific timeframe. Sets are tracked as `Milestones`.
- Resource: An informative asset, such as an eBook or report, that is often gated.

### Content stage defintions

- Awareness (Top): Buyer is aware of a need or desire. Content at this stage should establish brand familiarity and trust by unbiasly helping the consumer learn about their problem, desire, or need.
- Consideration (Middle): Buyer is considering options and alternatives to acheive their need or desire. Content at this stage should help the buyer understand what GitLab is, it's capabilities, and how it compares to alternatives.
- Purchase (Bottom): Buyer is ready to purchase a product. Content at this stage should enable the consumer to easily make a purchase. For our buyers, this might include product documentation.

### Planning timeline

Pillar strategy is planned annually and reviewed quarterly. Content sets are planned quarterly and reviewed monthly. When planning, we follow the 80/20 rule: 80% of capacity is planned leaving 20% unplanned to react to last minute opportunities.

- **2 weeks prior to start of the quarter:** Proposed content plans are published to the content schedule. Product marketing, digital marketing, and corporate events marketing give feedback on the plan.
- **1 week prior to the start of the quarter:** Kickoff calls are held.
- **1st day of the quarter:** Content plans are finalized.
- **1st of each month:** Progress reviews are held. Plans are adjusted if needed.
- **Last day of the quarter:** Cross-functional retrospective is held.

## Content library

Content for us is stored in PathFactory. To request access to PathFactory, submit an access request form. 

The content library in PathFactory can be filtered by content type, funnel stage, and topic. Topics are listed and defined in the [digital marketing programs management handbook](/handbook/marketing/revenue-marketing/digital-marketing-programs/digital-marketing-management/).

Published content should be shared to the #content-updates channel and added to PathFactory with the appropriate tags.

## What is a content pillar?

A content pillar is a go to market content strategy that is aligned to a high-level theme (for example, Just Commit) and executed via sets. For example, "Just commit to application modernization" is a content pillar about improving application infrastructure in order to deploy faster. Within this pillar, many topics can be explored (CI/CD, cloud native, DevOps automation, etc.) and the story can be adapted to target different personas or verticals.  

We use content pillars to plan our work so we can provide great digital experiences to our audiences. The content team aligns to themes to ensure we are executing strategically and meeting business goals. Pillars allow us to narrow in on a specific topic and audience, and sets help us break our work into more manageable compontents. Each set created should produce an end-to-end content experience (awareness to decision) for our audience.

![Content pillar diagram](/images/handbook/marketing/corporate-marketing/content-pillar.png)

#### What's included in a content set?

Here's an example of what's included in a content set:

| Quantity | Stage | Content Type | DRI |
| ------ | ------ | ------ | ------ |
| 4 | Awareness | Thought leadership blog post | Content marketing |
| 1 | Awareness | Topic webpage | Content marketing |
| 1 | Awareness | Resource | Content marketing |
| 4 | Consideration| Technical blog post | Content marketing |
| 1 | Consideration | Whitepaper | Product & technical marketing |
| 1 | Consieration | Solution page | Content & product marketing |
| 2 | Consideration | Webcast | Product & technical marketing |
| 1 | Purchase | Demo | Technical marketing |
| 1 | Purchase | Data sheet | Product marketing |


**Sources:**

- [What Is a Content Pillar? The Foundation for Efficient Content Marketing](https://kapost.com/b/content-pillar/)
- [How to Create Pillar Content Google Will Love](https://contentmarketinginstitute.com/2018/04/pillar-content-google/)
- [What Is a Pillar Page? (And Why It Matters For Your SEO Strategy)](https://blog.hubspot.com/marketing/what-is-a-pillar-page)
- [Content for Each Buying Stage of the Consumer Purchase Cycle](https://contentwriters.com/blog/content-consumer-purchase-cycle/)
