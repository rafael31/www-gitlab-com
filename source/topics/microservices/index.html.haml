---
title: What are microservices?
description: "Microservices are a modern software architecture design where an application is split into many small services allowing teams to innovate faster and achieve massive scale."
suppress_header: true
extra_css:
  - auto-devops.css
  - product.css
  - resources.css
extra_js:
  - features.js
---

.blank-header
  %img.image-border.image-border-left{ src: "/images/home/icons-pattern-left.svg", alt: "Gitlab hero border pattern left svg" }
  %img.image-border.image-border-right{ src: "/images/home/icons-pattern-right.svg", alt: "Gitlab hero border pattern right svg" }
  .header-content
    %img.hero-image-small{ src: "/images/devops-tools/gitlab-logo.svg", alt: "Gitlab logo svg" }
    %h1 What are microservices?
    %p
      A microservices architecture splits an application into many small services allowing teams to innovate faster and achieve massive scale.
    %a.btn.cta-btn.accent.just-commit{ href: "/webcast/cloud-native-transformation/" } Join our cloud transformation webcast

.toc-links
  %a{ href: "#from-monolith-to-microservices" } Overview
  %a{ href: "#components-of-a-microservice" } Componets
  %a{ href: "#engineering-benefits-of-microservices" } Engineering benefits
  %a{ href: "#business-value-of-microservices" } Business benefits
  %a{ href: "#using-gitlab-with-microservices" } GitLab and microservices
  %a{ href: "#additional-resources" } Additional resources

.content-container
  .content.tile
    :markdown
      ## Moving from monolith to microservices

      ![visualizing monolith vs microservice architecture](/images/microservices/monolith-vs-microservices.png)

      Microservices architecture is commonly understood by comparing it to a legacy "monolith" application architecture. With a monolithic architecture, all of the components are part of a single unit. Everything is developed, deployed, and scaled together. The app must be written in a single language, with a single runtime. Different teams working on different parts of the app need to coordinate in order to make sure they don't affect each other's work. For example, one part of the app may depend on a module that needs a specific version say 1.8, which another teams needs the same module, but requires the 1.7 version because 1.7 is incompatible with another dependency. In a monolithic app you have to pick one or the other. Similarly, everything is deployed as a single application so code must be tested and deployed together.
      
      With microservices, each component is broken out and deployed individually as services and the services communicate with each other via API calls.

  .content.tile
    :markdown
      ## Components of a microservice

      While every organization defines microservices differently, [Martin Fowler's quintessential article on microservices](https://martinfowler.com/articles/microservices.html) defines 9 components that most microservice architectures have in commons.

      1. Componentization via Services
      2. Organized around Business Capabilities
      3. Products not Projects
      4. Smart endpoints and dumb pipes
      5. Decentralized Governance
      6. Decentralized Data Management
      7. Infrastructure Automation
      8. Design for failure
      9. Evolutionary Design

      Fowler went into more detail about each of these components in this talk from GOTO.

    %p
      <iframe width="853" height="480" src="https://www.youtube.com/embed/wgdBVIX9ifA" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

  .content.tile
    :markdown
      ## Engineering benefits of microservices

      Building applications with a microservices architecture, or decomposing a legacy monolith into microservices increases velocity, flexibility, and scalability, at the cost of simplicity. Monoliths are simple to build, deploy, and debug, but are very hard to scale. By using a more complex microservice architecture engineering organizations gain several benefits.

      1. Services can scale independently.
      2. Individual services can fail without taking down the entire application.
      3. Teams can choose their own technology stack to that can be different from other services.
      4. Functionality can be tested and swapped more easily.
      5. Developer productivity: individual teams can move faster.

  .content.tile
    :markdown
      ## Business value of microservices

      1. Faster pace of innovation.
      2. Greater stability/resiliency (less loss of revenue due to downtime)
      3. Greater scale, the software is able to keep up with business demand.
      4. Lower costs. Since infrastructure can be tailored to specific services less overall infrastructure is needed to run the application saving costs.

  .content.tile
    :markdown
      ## Using GitLab with microservices

      With GitLab you can commit your code and have all the tools you need integrated in a single application. You won't have to stitch together 10 tools for every project.
      
      Using an integrated solution to manage your microservices helps you avoid having engineers siloed off with their respective teams and tools. Creating visibility among teams and getting rid of the need for handoffs leads to a faster DevOps lifecycle while also ensuring that your projects deploy and remain stable.

      A few ways GitLab simplifies microservice orchestration include...

      1. [Built-in CI/CD](https://about.gitlab.com/product/continuous-integration/): As Fowler points out, infrastructure automation using continuous delivery and deployment is necessary for microservices. GitLab's build-in CI/CD is ideal for businesses looking to leverage microservices.
      2. [Built-in container registry](https://docs.gitlab.com/ee/user/project/container_registry.html) and tight [Kubernetes integration](https://about.gitlab.com/solutions/kubernetes/): While microservices architecture can be used with legacy VM technology, containers and Kubernetes make building microservices significantly easier. GitLab is designed to work well with Kubernetes.
      3. [Built-in Monitoring](https://about.gitlab.com/stages-devops-lifecycle/monitor/): Monitoring is critical to successful operation. GitLab built-in monitoring capabilities leveraging Prometheus make GitLab ideal for microservices.
      4. [Multi-project pipelines](https://docs.gitlab.com/ee/ci/multi_project_pipelines.html) support running pipelines with cross-project dependencies
      5. Monorepo support with the ability to [run a pipeline only when code in a specific directory changes](https://docs.gitlab.com/ee/ci/yaml/#only-and-except-simplified).
      5. [Group-level Kubernetes clusters](https://docs.gitlab.com/ee/user/group/clusters/) allow multiple projects to integrate with a single Kubernetes cluster.

      ## Getting even better

      While GitLab is great for microservices today, there are several features on the roadmap to make it even better.

      - [Global docker registry](https://gitlab.com/gitlab-org/gitlab-ce/issues/49336)
      - [First class container builds](https://gitlab.com/gitlab-org/gitlab-ce/issues/48913)
      - [Define multiple pipelines in single .gitlab-ci.yml](https://gitlab.com/gitlab-org/gitlab-ce/issues/22972)

  .content.title
    = partial "includes/just-commit/cta/ask-com"

  .content.tile
    :markdown
      ## Additional resources

      - [How to break a Monolith into Microservices](https://martinfowler.com/articles/break-monolith-into-microservices.html)
      - [Mastering Chaos - A Netflix Guide to Microservices](https://www.youtube.com/watch?v=CZ3wIuvmHeM)
      - [Evolution of business logic from monoliths through microservices, to functions](https://read.acloud.guru/evolution-of-business-logic-from-monoliths-through-microservices-to-functions-ff464b95a44d)
      - [Guilt's move from monolith to microservices](https://www.youtube.com/watch?v=C4c0pkY4NgQ)
      - [benefits and drawbacks of microservices](https://about.gitlab.com/2016/08/16/trends-in-version-control-land-microservices/)
      - [The microservices repo explosion](https://about.gitlab.com/2018/11/26/microservices-integrated-solution/)

