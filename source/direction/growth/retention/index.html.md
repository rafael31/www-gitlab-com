---
layout: markdown_page
title: Product Vision - Retention
---

### Overview

The Retention Team at GitLab focuses on uncovering and addressing the leading reasons for subscription cancellation through hypothesis based testing and iteration. We will analyze churn from both a revenue and net customer perspective and will use that information to inform prioritization. 

We will gain a deep understanding of the top reasons for churn by leveraging qualitative data from our customers and by partnering with Customer Success and Customer Support. In addition to the qualitative data we will analyze quantitative data to understand the key signals that indicate an account is at-risk or successful. 

Our testing will likely focus on identifying at-risk customers and guiding them back to successful. 

Our goal is to keep teams happy and engaged with GitLab so they can continue to realze and benefit from the value that GitLab provides.  

**Retention Team**

Product Manager: [Michael Karampalas](https://about.gitlab.com/company/team/#mkarampalas) | Engineering Manager: [Phil Calder](https://about.gitlab.com/company/team/#pcalder) | UX Manager: [Jacki Bauer](https://about.gitlab.com/company/team/#jackib) | Product Designer: [Kevin Comoli](https://about.gitlab.com/company/team/#kcomoli) | Full Stack Engineer: [Jeremey Jackson](https://about.gitlab.com/company/team/#jejacks0n) | Full Stack Engineer: [Jay Swain](https://about.gitlab.com/company/team/#jayswain)

**Retention Team Mission**

*   Prevent customers from leaving GitLab

**Retention KPI**

Gross Retention

Measured by gross retention. Gross retention is defined as:
Gross Retention (%) = (min(B, A) / A) * 100%

* A. MRR 12 months ago from currently active customers

* B. Current MRR from the same set of customers as A.


_Supporting performance indicators:_

* 30/90/180/360 day MAU retention
* % of subscriptions with auto-renew enabled 

### Problems to solve

Do you have challenges? We’d love to hear about them, how can we help GitLab contributors stay happy and engaged with GitLab? 

* The renewal process is confusing and clunky. We need to make it easy for our self-hosted and gitlab.com users to renew. 
* There are a lot of manual processes and effort involved in our renewals, including accounts that use "auto-renew". We need to streamline and automate as much of the renewal process as possible so that our GitLab team members can focus their time and energy on making our customers successful.
* We have users that disengage with our product for various reasons. We need to gain a better understanding of those reasons and work in conjunction with Customer Success to help get those users back on track and experiencing the full value of the GitLab offering. 


### Our approach

Retention runs a standard growth process. We gather feedback, analyze the information, ideate, then create experiments and test. As you can imagine the amount of ideas we can come up with is enormous so we use the [ICE framework](https://blog.growthhackers.com/the-practical-advantage-of-the-ice-score-as-a-test-prioritization-framework-cdd5f0808d64) (impact, confidence, effort) to ensure we are focusing on the experiments that will provide the most value. 


### Maturity

The growth team at GitLab is new and we are iterating along the way. As of August 2019 we have just formed the Retention group and are planning to become a more mature, organized, and well oiled machine by January 2020. 


### Helpful Links

[GitLab Growth project](https://gitlab.com/gitlab-org/growth)

[KPIs & Performance Indicators](https://about.gitlab.com/handbook/product/metrics/)

Reports & Dashboards