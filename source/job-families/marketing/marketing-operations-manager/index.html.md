---
layout: job_family_page
title: "Marketing Operations Manager"
---

You understand that setting a foundation for healthy growth in a fast-paced company is effective marketing operations. Your job of helping the rest of the marketing team be successful is three-fold:  evaluating/selecting/customizing technology to enable effective marketing at GitLab, ensuring high data quality and helping colleagues access that data to enable smarter decisions, and assisting in marketing analysis/planning/strategy.

The team is structured in a way to support individual functional groups within Marketing. Each manager will be the point person for a given group to provide operational guidance to the team/s, implement/ streamline related processes, and troubleshoot any issues that arise. In addition, each manager will be the subject matter expert (SME) on various tools generally related to their functional group.

## Responsibilities

### Marketing technology
  * Evaluate new marketing technology that can enable GitLab to grow its business faster and more efficiently.
  * Create documentation that guides the marketing team in their use of marketing software tools.
  * Train the marketing team on marketing software tools.
  * Audit use of marketing software tools with an eye towards continually improving how they are configured.

### Marketing data stewardship
  * Ensure we have documented processes in place that facilitate accurate data collection.
  * Review data quality across key dimensions that GitLab uses to evaluate its marketing performance.
  * Where data quality is lacking, identify the root cause and address systematically.

### Marketing analysis
  * Measure the marketing department's contribution to sales pipeline, and assess their performance throughout the entire funnel.
  * Measure the effectiveness of marketing campaigns and content, including ROI of marketing campaigns.
  * Measure the ratio of customer acquisition cost to customer lifetime value, by marketing tactic.
  * Assist with data-driven budgeting, planning, and strategy.

## Requirements

* Excellent spoken and written English.
* Experience with marketing automation software (Marketo highly preferred).
* Experience with CRM software (Salesforce preferred).
* For specializations, we are looking for deeper B2B software experience related to:
  * Sales Development / Business Development organization and related tools - Outreach, LeanData, and data enrichment software.
  * Field and Corporate events, plus Marketo, Eventbrite and swag vendors.
  * Digital Marketing Programs, with Bizible, PathFactory, Google Analytics, Sprout Social to name a few.
* Experience with Open Source software and the developer tools space is preferred.
* Proficiency in MS Excel / Google Sheets.
* Is your college degree in French foreign politics with a minor in interpretive dance but you’ve been selling and marketing products since you were 12? Perfect. We understand that your college degree isn’t the only thing that prepares you as a potential job candidate.
* You are obsessed with making customers happy. You know that the slightest trouble in getting started with a product can ruin customer happiness.
* Be ready to learn how to use GitLab and Git
* You share our [values](/handbook/values), and work in accordance with those values.
* [Leadership at GitLab](/handbook/leadership/#management-group)


## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team/).
* Qualified candidates will be invited to schedule a 15 minute [screening call](/handbook/hiring/interviewing/#conducting-a-screening-call) with one of our Global Recruiters after completing our questionnaire provided.
* A 30 minute interview with future co-worker/s (Marketing Operations Manager)     
* A 45 minute interview with future manager (Director, Marketing Operations)
* A 30 minute interview with future marketing partner/s based on specialization
* Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring).

## Relevant Links
[Marketing Handbook](/handbook/marketing)
