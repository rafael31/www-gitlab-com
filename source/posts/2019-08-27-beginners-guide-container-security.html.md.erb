---
title: "A beginner’s guide to container security"
author: Vanessa Wegner
author_gitlab: vwegner
author_twitter: gitlab
categories: insights
image_title: '/images/blogimages/container-security-beginners.jpg'
description: "Container adoption is growing, but traditional security methods can’t keep up."
Tags: CI/CD, DevOps, kubernetes, security
cta_button_text: 'Join us at our first user conferences in Brooklyn and London!'
cta_button_link: '/events/commit/'
twitter_text: "A beginner’s guide to container security"
postType: content marketing
---

Containers are a helpful tool for developing and shipping apps in the cloud.
They’re resource-efficient for managing application infrastructure, but as
with any new tool in your tech stack, containers introduce new attack surfaces
that put your business at risk. [58% of developers report to Forrester that their companies currently use containers or plan
to use containers in the next 12 months](https://www.forrester.com/report/Now+Tech+Container+Security+Q4+2018/-/E-RES142078#read-full) – but many also said that security was a primary inhibitor to container adoption. To combat this, teams should shift
left by integrating security practices into each stage of the application
lifecycle.

## Securing your containers

Container security differs from traditional security methods due to the
increased complexity and dynamism of the container environment. Simply put,
there are a lot more moving pieces. Container security comprises everything
from the applications they contain to the infrastructure they run on. Base
image security and quality are critical to ensure that any derivative images
come from a trusted source. [RedHat recommends building security into your
container pipeline](https://www.redhat.com/en/topics/security/container-security)
by gathering trusted images, managing access with the use of a private registry,
integrating security testing and automating deployment, and continuously
defending your infrastructure.

## Challenges to container security

While containers seem to behave like small virtual machines (VMs), they actually
don’t – and so require a different security strategy. Traffic between apps in a
container does not cross perimeter network security, but should be monitored for
malicious traffic between apps and their images. Your orchestrator can be used
to set security policies for processes and resources, but a complete security
strategy requires more.

### Each architectural layer of a container has its own security needs

Container images define what runs in each container. Developers should make
sure that images don’t contain any vulnerabilities, and should avoid creating
extraneous images to minimize the container’s attack surface. Image validation
tooling can be used to forbid untrusted images, but is often not enabled by
default. Images can also be scanned after they’re built to detect dependent images that
might also have vulnerabilities, according to a [Forrester report](https://www.forrester.com/report/Ten+Basic+Steps+To+Secure+Software+Containers/-/E-RES136406).

Unlike VMs, multiple containers can run on the same operating system and an
attack can happen at either level. A vulnerable host OS puts its containers at
risk, and a vulnerable container can open an attack pathway to the host OS.
[Security professionals in a Forrester report recommend
enforcing namespace isolation](https://www.forrester.com/report/Ten+Basic+Steps+To+Secure+Software+Containers/-/E-RES136406) to limit interaction between container and host
OS kernel, and automating patching to align with vendor patch releases. The OS
should also be as simple as possible, free from unnecessary components (such as
apps or libraries that aren’t actually needed to run your orchestrator).

Container orchestration coordinates and manages containers, allowing
containerized applications to scale and support thousands of users. Your
orchestrator may have its own out-of-the-box security features; for example,
[Twistlock highlights
Kubernetes’ “pod security policies”](https://www.twistlock.com/2019/06/06/5-kubernetes-security-best-practices/), which allow you to set rules for Kubernetes
to automatically enforce across all pods within the cluster. This type of basic
feature is useful, but is also only a first step towards a more robust set of
policies.

### Access controls and storing secrets

Containers may be spread across multiple systems and cloud providers, making
access management all the more important. Secrets, which include API keys,
login credentials, and tokens, should be stringently managed to ensure container
access is limited to privileged users. User access can also be defined by
role-based access control, allowing you to limit access to an as-needed basis.

### Runtime security

Once deployed, container activity must be monitored and teams need to be able to detect
and respond to any security threats. [Nordcloud suggests
monitoring for suspicious behaviors](https://nordcloud.com/container-security-how-to-differ-from-the-traditional/) such as network calls, API calls, and
unusual login attempts. Teams should have predefined mitigation steps for pods,
and should be able to isolate the container on a different network, restart it,
or stop it until the threat is identified.

### Cloud native requires greater accountability

Cloud providers also take on some of the work by securing the underlying
hardware and basic networks used to provide cloud services, but users still
share that responsibility, as well as assuming responsibility for application
access, app configuration and patching, and system patching and access.

## The NIST guidelines for container security

In 2017, the U.S. Department of Commerce published its [Application Container
Security Guide](https://nvlpubs.nist.gov/nistpubs/SpecialPublications/NIST.SP.800-190.pdf).
Here are the key recommendations:

#### Tailor the organization’s operational culture and technical processes to support the new way of developing, running, and supporting applications made possible by containers.

Adopting containers might be disruptive to your existing culture and
development methodologies, and your current practices might not be directly
applicable in a containerized environment. Encourage, educate, and train your
team to rethink how they code and operate.

#### Use container-specific host OSs instead of general-purpose ones to reduce attack surfaces.

A container-specific host OS is a minimalist OS designed to only run
containers. Using these OSs greatly reduces attack surfaces, allowing fewer
opportunities for your containers to be compromised.

#### Only group containers with the same purpose, sensitivity, and threat posture on a single host OS kernel to allow for additional in-depth defense.

 Segmenting containers provides additional defense in-depth. Grouping
containers in this manner makes it more difficult for an attacker to expand
potential compromises to other groups. It also increases the likelihood that
compromises will be detected and contained.

#### Adopt container-specific vulnerability management tools and processes for images to prevent compromises.

Traditional tools make many assumptions that are misaligned with a
containerized model, and are often unable to detect vulnerabilities within
containers. Organizations should adopt tools and processes to validate and
enforce compliance with secure configuration best practices for images –
including centralized reporting, monitoring each image, and preventing
non-compliant images from being run.

#### Consider using hardware-based countermeasures to provide a basis for trusted computing.

Extend security practices across all tiers of the container technology by
basing security on a hardware root of trust, such as the [Trusted Platform Model (TPM)](https://whatis.techtarget.com/definition/trusted-platform-module-TPM).

#### Use container-aware runtime defense tools.

Deploy and use a dedicated container security solution capable of monitoring
the container environment and providing precise detection of anomalous and
malicious activity within it.

## Integrate security to increase DevOps velocity

Container adoption will continue to grow, as will the need to secure them. The
most efficient way to ensure security at scale is to integrate security
functions and procedures into each phase of development and deployment. As the
technologies mature, vendors will roll out new security features and
requirements, and potentially overhaul outdated features. It’s
important to pay attention to release updates, take note of security changes,
and continually update your container security posture to keep your business
and users out of harm’s way.
